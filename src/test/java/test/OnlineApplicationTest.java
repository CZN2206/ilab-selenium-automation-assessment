package test;


import PageObjects.IlabPageObject;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Test;
import org.openqa.selenium.*;
import utils.DriverSetup;
import utils.ExcelUtils;
import utils.ScreenshotUtil;
import utils.ExtentReport;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

public class OnlineApplicationTest extends DriverSetup {


    private String[][] data = null;
    private String name;
    private String email;
    IlabPageObject ele = new IlabPageObject();

    @Test
    public void OnlineApplicationTest() throws Exception{

        // reading test data from the file
        data = excelUtils.readExcelDataFileToArray(fileName, "PersonalDetails");
        String[] headers = ExcelUtils.getColumHeaders(data);
        String phone = "0";

        for(int i=0; i < 9; i++){
            phone = phone + (int)(Math.random()*9+1);
        }

        ExtentReport.getInstanceExport().startTest("Assessment Test");
        
        for(int i = 1; i < data.length; i++){

            name = ExcelUtils.getCellValue(data[i], headers, "Name");
            email = ExcelUtils.getCellValue(data[i], headers, "Email");
        }


      // Online job application test
        try{
            WebElement careers= driver.findElement(By.xpath(ele.CareerButton()));
            careers.click();
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Career Button Clicked");
        }
        catch (Exception error){
            //Screenshot for the error
            ScreenshotUtil.captureScreenshots(driver,"careers");
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when clicking Button "+error.getMessage());
            System.out.print(error.getMessage());
        }
        try{
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
            WebElement country= driver.findElement(By.cssSelector(ele.CountryButton()));
            country.click();
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Country Selected");
        }
        catch(Exception error){
            ScreenshotUtil.captureScreenshots(driver,"country selected");
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when selecting Country "+error.getMessage());

            System.out.print(error.getMessage());
        }
        try{
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
            WebElement openPositions=driver.findElement(By.cssSelector(ele.availableJobButton()));
            openPositions.click();
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Open positions clicked");
        }
        catch(Exception error){
            ScreenshotUtil.captureScreenshots(driver,"available job selected");
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when selecting job "+error.getMessage());

            System.out.print(error.getMessage());
        }
        try{
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
            WebElement applyOnline=driver.findElement(By.cssSelector(ele.applyOnlineButton()));
            applyOnline.click();
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Open positions clicked");
        }
        catch(Exception error){
            ScreenshotUtil.captureScreenshots(driver,"apply online button");
            System.out.print(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error clicking apply online  "+error.getMessage());

        }
        try{
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
            WebElement userName=driver.findElement(By.id("applicant_name"));
            userName.sendKeys(name);
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Applicant name entered");
        }
        catch(Exception error){
            ScreenshotUtil.captureScreenshots(driver,"applicant name");
            System.out.print(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when entering applicant name  "+error.getMessage());

        }
        try{
            WebElement emailAdress=driver.findElement(By.id(ele.applicantEmail()));
            emailAdress.sendKeys(email);
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Applicant email entered");
        }
        catch(Exception error){
            ScreenshotUtil.captureScreenshots(driver,"email");
            System.out.print(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when entering applicant email  "+error.getMessage());

        }
        try {
            WebElement phoneNumber = driver.findElement(By.id(ele.applicantPhone()));
            phoneNumber.sendKeys(phone);
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Applicant phone entered");
        }
        catch(Exception error){
            ScreenshotUtil.captureScreenshots(driver,"phone");
            System.out.print(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when entering applicant phone  "+error.getMessage());

        }
        try {
            WebElement sendApplicationButton = driver.findElement(By.cssSelector(ele.sendApplicationButton()));
            sendApplicationButton.click();
            ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Send application clicked");
        }
        catch(Exception error){
            ScreenshotUtil.captureScreenshots(driver,"send  application");
            System.out.print(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Error when entering applicant phone  "+error.getMessage());

        }

        try {
            boolean isTheTextDisplayed = driver.getPageSource().contains("You need to upload at least one file.");
               assertTrue(isTheTextDisplayed);
                
            	if(isTheTextDisplayed == true) {
                ExtentReport.getInstanceExtentTest().log(LogStatus.PASS, "Validation Passed on upload file");
            	}else if(isTheTextDisplayed == false) {
            		
            		ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Validation Failed on upload file");	
            	}
            	
            	
                ExtentReport.getInstanceExport().flush();
                System.out.println("Report written");
        }
        catch(Exception error){
            ScreenshotUtil.captureScreenshots(driver,"error message");
            System.out.print(error.getMessage());
            ExtentReport.getInstanceExtentTest().log(LogStatus.FAIL, "Validating file upload  "+error.getMessage());

        }
    }



}
