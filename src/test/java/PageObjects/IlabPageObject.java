package PageObjects;

public class IlabPageObject {

    public static final String careers ="/html/body/header/div/div/div[3]/nav/ul/li[4]/a";
    public static final String country ="div.vc_btn3-container:nth-child(9)";
    public static final String availableJob ="div.wpjb-grid-row:nth-child(1) > div:nth-child(2) > span:nth-child(1) > a:nth-child(1)";
    public static final String applyOnline = ".wpjb-form-toggle";
    public static final String applicantName  = "applicant name";
    public static final String applicantEmail = "email";
    public static final String applicantPhone = "phone";
    public static final String sendApplication = "#wpjb_submit";

    public String CareerButton(){
        return careers;
    }
    public String CountryButton(){
        return country;
    }
    public String availableJobButton(){
        return availableJob;
    }
    public String applyOnlineButton(){
        return applyOnline;
    }
    public String applicantName(){
        return applicantName;
    }
    public String applicantEmail(){
        return applicantEmail;
    }
    public String applicantPhone(){
        return applicantPhone;
    }
    public String sendApplicationButton(){
        return sendApplication;
    }

}

