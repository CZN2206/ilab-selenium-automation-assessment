package utils;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverSetup {

    public static WebDriver driver = null;
    public ExcelUtils excelUtils;
    public String fileName;
    public String	Browsertype;
    public String URLvalue; 

    //opening the browser and website before test
    @Before
    public void setup() throws Exception{
        excelUtils = new ExcelUtils();
        fileName = "src/test/resources/TestData.xlsx";    	
    	
    	excelUtils.EnvironmentValue();
    	
    	
    	Browsertype = excelUtils.getBrowser();
    			//excelUtils.EnvironmentValue("Chrome");
    	
        if(Browsertype.equals("Chrome")) {
        	//set properties for web driver
            System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe");
            //instantiate drivers
            driver = new ChromeDriver();        	
        }else if(Browsertype.equals("Firefox")) {
        //Part for opening Firefox	
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe");	
        driver = new FirefoxDriver();
        } else if(Browsertype.equals("IE")) {
        //Part for opening IE	
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe");	
        driver = new FirefoxDriver();	
        
        	// driver = new FirefoxDriver();
        } 
       

        URLvalue = excelUtils.getURLvalue();
        		//excelUtils.EnvironmentValue("URL");
        driver.manage().window().maximize();
        
        driver.get(URLvalue);



    }

    //closing the browser after the test
    @After
    public void endTest(){
        driver.close();
    }
}
